import '../scss/app.scss';

/* Your JS Code goes here */

/* Loader */

window.addEventListener("load", () => {
    const loader = document.querySelector(".loader");
    loader.className += " hidden";
  });


new Splide( '.splide' ).mount();


/* Scroll */

AOS.init();


const bodyPage = document.getElementById('bodyPage');
const overlayPage = document.getElementById('overlay-page');
const btnMenu = document.getElementById('btnMenu');
const btnMenuClose = document.getElementById('btnMenuClose');
const menuPage = document.getElementById('menuNav');

btnMenu.addEventListener('click', () => {
    overlayPage.classList.toggle('show');
    menuPage.classList.toggle('open');
    bodyPage.classList.toggle('overflow-hidden');
})

btnMenuClose.addEventListener('click', () => {
    overlayPage.classList.toggle('show');
    menuPage.classList.toggle('open');
    bodyPage.classList.toggle('overflow-hidden');
})


/* ANchors */

document.querySelectorAll('a[href^="#"]').forEach(anchor => {
    anchor.addEventListener('click', function (e) {
        e.preventDefault();
        document.querySelector(this.getAttribute('href')).scrollIntoView({
            behavior: 'smooth'
        });
    });
});



/* Scroll */

let lastScroll = 0;

window.addEventListener('scroll', ()=> {
    const currentScroll = window.scrollY


    if( currentScroll <= 0) {
        bodyPage.classList.remove('scroll-up');
    }

    if(currentScroll > lastScroll && !bodyPage.classList.contains('scroll-down')) {
        bodyPage.classList.remove('scroll-up');
        bodyPage.classList.add('scroll-down');
    }

    if(currentScroll < lastScroll && bodyPage.classList.contains('scroll-down')) {
        bodyPage.classList.remove('scroll-down');
        bodyPage.classList.add('scroll-up');
    }


    lastScroll = currentScroll;
})




/* Demo JS 
import './demo.js';*/
